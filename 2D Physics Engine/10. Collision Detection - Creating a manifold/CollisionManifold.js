class ContactManifold{
	constructor(depth,normal,penetrationPoint){
		this.depth = depth;
		this.normal = normal;
		this.penetrationPoint = penetrationPoint;		
	}
	
	//we need this method later
	resolveCollision(){
		
	}
	
	// also for later use
	positionalCorrection(){
		
	}
	
	draw(ctx){
		
	}
}