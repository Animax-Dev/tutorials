class Playground{
    constructor(){
        this.simulation = new Simulation();
    }


    update(dt){
        this.simulation.update(dt);
    }

    draw(){
        this.simulation.draw();

        DrawUtils.drawLine(new Vector2(0, 0), new Vector2(100, 100), "black", 10);
        DrawUtils.drawPoint(new Vector2(100, 100), 20, "blue");
        DrawUtils.strokePoint(new Vector2(100, 100), 20, "red");
        DrawUtils.drawRect(new Vector2(200, 200), new Vector2(100, 100), "green");
        DrawUtils.drawText(new Vector2(300, 300), 20, "black", "Hello World!");

    }



    onMouseMove(position){
        position.Log();
    }

    onMouseDown(button){
        console.log("Mouse button pressed: " + button);
    }

    onMouseUp(button){
        console.log("Mouse button released: " + button);
    }

}