class FluidHashGrid{
    constructor(cellSize){
        this.hashMap = new Map();
		this.particles = [];
		this.hashMapSize = 1000000;
		this.p1Prime = 6614058611;
		this.p2Prime = 7528850467;
		this.cellSize = cellSize;
    }

    initialize(particles){
        this.particles = particles;
    }

    clearGrid(){
        this.hashMap.clear();
    }

	cellIndexToHash(x, y){
		let hash = (x * this.p1Prime ^ y * this.p2Prime) % this.hashMapSize
		return hash;
	}
	
	getGridIdFromPos(pos){
		let x = parseInt(pos.x / this.cellSize);
		let y = parseInt(pos.y / this.cellSize);
		return this.cellIndexToHash(x,y);
	}

    getContentOfCell(id){
		let content = this.hashMap.get(id);
		
		if(content == null) return [];
		else return content 
	}
    
    mapParticlesToCell(){
		for(let i=0; i<this.particles.length; i++){
			let pos = this.particles[i].position;
			let x = parseInt(pos.x / this.cellSize);
			let y = parseInt(pos.y / this.cellSize);			
			let hash = this.cellIndexToHash(x,y);
			let entries = this.hashMap.get(hash);
			if(entries == null){
				let newArray = [this.particles[i]];
				this.hashMap.set(hash, newArray);
			}else{
				entries.push(this.particles[i]);
			}
        }
    }
}