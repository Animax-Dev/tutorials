class Playground{
    constructor(){
        this.simulation = new Simulation();
        this.mousePos = new Vector2(0,0);
    }


    update(dt){
        this.simulation.update(dt, this.mousePos);
    }

    draw(){
        this.simulation.draw();
    }



    onMouseMove(position){
        this.mousePos = position;
    }

    onMouseDown(button){
        console.log("Mouse button pressed: " + button);
    }

    onMouseUp(button){
        console.log("Mouse button released: " + button);
    }

}